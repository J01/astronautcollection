#include "GameGeometry.h"


void Square::Create(Dim2D& pnt, float& len)
{
    vertices[0] = pnt;
    vertices[1] = { pnt.x + len, pnt.y };
    vertices[2] = { pnt.x + len, pnt.y + len };
    vertices[3] = { pnt.x, pnt.y + len };
}
void Square::Translate(Dim2D& dist)
{
    for (int i = 0; i < 4; i++)
    {
        vertices[i] = vertices[i] + dist;
    }
}


void Circle::Create(Dim2D cnt, float rad)
{
    centre = cnt;
    radius = rad;
    radSq = rad * rad;
}
void Circle::Translate(Dim2D& dist)
{
    centre = centre + dist;
}


bool Collides(Circle& cl1, Circle& cl2)
{
    float maxDist = powf(cl1.radius + cl2.radius, 2);
    float dist = powf(cl1.centre.x - cl2.centre.x, 2) + powf(cl1.centre.y - cl2.centre.y, 2);
    return (dist <= maxDist);
}
bool Collides(Circle& cl1, Dim2D& pnt)
{
    float radSq = cl1.radius * cl1.radius;
    float dist = powf(pnt.x - cl1.centre.x, 2) + powf(pnt.y - cl1.centre.y, 2);
    return (dist < radSq);
}
bool Collides(Square& sq, Circle& cl)
{
    //get absolute difference between centres
    Dim2D centresDist;
    centresDist.x = fabs(cl.centre.x - 0.5f*(sq.vertices[0].x + sq.vertices[1].x));
    centresDist.y = fabs(cl.centre.y - 0.5f*(sq.vertices[0].y + sq.vertices[3].y));

    bool ret;
    float width;
    width = sq.vertices[1].x - sq.vertices[0].x;

    //if the centre is further away than radius + dist to edge, then it wont be intersecting
    if ((centresDist.x > (width / 2 + cl.radius)) || (centresDist.y > (width / 2 + cl.radius)))
    {
        ret = false;
    }
    //if the radius is centre is within the circle
    else if ((centresDist.x <= (width/2)) || (centresDist.y <= (width/2)))
    {
        ret = true;
    }
    else
    {
        //finds the distance between the corner and the circle centre
        float cornDistSq = powf(centresDist.x - width / 2, 2) + powf(centresDist.y - width / 2, 2);
        ret = (cornDistSq <= cl.radSq);
    }
    return ret;
}
bool Collides(Square& sq1, Square& sq2)
{
    unsigned int i = 4;
    bool ret = false;

    while (i && !ret)
    {
        i--;
        ret = Collides(sq1, sq2.vertices[i]);
    }

    return ret;
}
bool Collides(Square& sq1, Dim2D& pnt)
{
    bool ret = false;

    if ((pnt.x >= sq1.vertices[0].x && pnt.x <= sq1.vertices[2].x) && (pnt.y >= sq1.vertices[0].y && pnt.y <= sq1.vertices[2].y))
    {
        ret = true;
    }
    return ret;
}