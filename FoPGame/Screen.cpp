#include "Screen.h";

void Screen::Initialise(Dim2D& sz)
{
    size = sz;

    if (!tex.loadFromFile(ScreenConsts::path))
        assert(false);
    tex.setRepeated(true);
    spr.setTexture(tex);
    spr.setScale(size.x / tex.getSize().x, size.y / tex.getSize().y);
    spr.setPosition(0, 0);
    topLeft = { -size.x / 2, - size.y / 2 };

    box.Create(topLeft, size.x);
}
void Screen::Update(Dim2D& ShipPos)
{
    topLeft = { ShipPos.x - (size.x / 2), ShipPos.y -(size.y / 2)  };
    sf::IntRect ir = spr.getTextureRect();
    ir.left = topLeft.x + (size.x / 2);
    ir.top = topLeft.y + (size.y / 2);
    spr.setTextureRect(ir);

    box.Create(topLeft, size.x);
}
void Screen::Draw(sf::RenderWindow& window)
{
    window.draw(spr);
}
void Screen::DrawRelative(sf::RenderWindow& window, sf::Sprite& spr, Dim2D& pos)
{
    Dim2D drawPos = pos - topLeft;
    spr.setPosition(drawPos.x, drawPos.y);
    window.draw(spr);
}


//bool Screen::isOnScreen(Dim2D pos) 
//{
//    if ((pos.x > topLeft.x && pos.x < bottomRight.y) && (pos.y > topLeft.y && pos.y < topLeft.y)) 
//    {
//        return true;
//    }
//    else
//    {
//        return false;
//    }
//}
//bool Screen::isOnScreen(Dim2D pos, Dim2D size)
//{
//    bool ret = false;
//    if (isOnScreen(pos))
//    {
//        ret = true;
//    }
//    else
//    {
//        float xDim, yDim;
//        xDim = size.x / 2.f;
//
//        if (((topLeft.x - xDim) < pos.x) && ((bottomRight.x + xDim) > pos.x))
//        {
//            yDim = size.y / 2.f;
//            if (((topLeft.y - yDim) < pos.y) && ((bottomRight.y + yDim) > pos.y))
//            {
//                ret = true;
//            }
//        }
//    }
//    return ret;
//}
//bool Screen::isOnScreen(Dim2D pos, float rad)
//{
//    bool ret = false;
//
//    if (isOnScreen(pos))
//    {
//        ret = true;
//    }
//    else
//    {
//
//    }
//
//
//    return ret;
//}