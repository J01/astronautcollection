#pragma once


#include <assert.h>
#include <math.h>
#include "Utils.h"

//these squares are only axis parallel
struct Square
{
    Dim2D vertices[4];

    //create a square based on a top left point and the side length
    void Create(Dim2D&, float&);
    //function to move the square
    void Translate(Dim2D&);
};


struct Circle
{
    float radius;
    float radSq;
    Dim2D centre;

    void Create(Dim2D cnt, float rad);
    void Translate(Dim2D&);
};

bool Collides(Square& sq1, Square& sq2);
bool Collides(Square& sq, Circle& cl);
bool Collides(Circle& c1, Circle& c2);
bool Collides(Square& sq1, Dim2D& pnt);
bool Collides(Circle& cl, Dim2D& pnt);

