#include "Astronauts.h"

void AstroCompass::Initialise()
{
    if (!tex.loadFromFile("data/sprites/arrow.png"))
        assert(false);
    spr.setTexture(tex);
    spr.setScale(size.x / tex.getSize().x, size.y / tex.getSize().y);
    spr.setOrigin((float)tex.getSize().x, (float)tex.getSize().y);
    spr.setPosition(centrePos.x, centrePos.y);
}
void AstroCompass::Update(std::vector<Astronaut>* astros, int num, Dim2D& ship)
{
    //only use city block distance for efficiency; it's good enough
    Astronaut* nearest = nullptr;
    int nearestDist;

    Astronaut* temp;
    int tempDist;

    for (int i = 0; i < num; i++)
    {
        temp = &(*astros)[i];
        if (temp->active)
        {

            //the first active element in vector becomes the nearest by default
            if (nearest == nullptr)
            {
                nearest = temp;
                nearestDist = static_cast<int>(abs(ship.x - temp->pos.x) + abs(ship.y - temp->pos.y));
            }
            //each following active is then compared and changed if necessary
            else
            {
                tempDist = static_cast<int>(abs(ship.x - temp->pos.x) + abs(ship.y - temp->pos.y));

                if (tempDist < nearestDist)
                {
                    nearestDist = tempDist;
                    nearest = temp;
                }
            }

        }
    }

    if (nearest)
    {
        Dim2D diff = nearest->pos - ship;
        float angle = diff.GetAngle();
        spr.setRotation(angle * 180.f / (float)PI + 90.f);
    }
}
void AstroCompass::Draw(sf::RenderWindow& window)
{
    window.draw(spr);
}

void Astronaut::Update(float& elapsed)
{
    angle += (float)turnSpeed * elapsed;
    if (angle > 360)
    {
        angle -= 360;
    }
    else if (angle < 0)
    {
        angle += 360;
    }
}

void AstronautCollection::Initialise(Screen& scrn, int& numToGen, World& wld)
{
    if (!sprSheet.loadFromFile(AstronautConsts::basePath))
        assert(false);

    if (!collPartTex.loadFromFile("data/particles/star.png"))
        assert(false);

    //load astronaut textures
    for (unsigned int i = 0; i < AstronautConsts::numTexs;i++)
    {
        ir[i].left = i * (int)AstronautConsts::texSize.x;
        ir[i].top = 0;
        ir[i].width = (int)AstronautConsts::texSize.x;
        ir[i].height = (int)AstronautConsts::texSize.y;
    }
    
    astroSpr.setTexture(sprSheet);
    astroSpr.setTextureRect(sf::IntRect(ir[0]));    
    astroSpr.setScale(AstronautConsts::AstronautSize.x / AstronautConsts::texSize.x, AstronautConsts::AstronautSize.y / AstronautConsts::texSize.y);
    astroSpr.setOrigin(AstronautConsts::texSize.x / 2, AstronautConsts::texSize.y / 2);

    //create astronaut vector
    astros.reserve(numToGen);
    int bnd = wld.playZone.upperBound;

    for (int i = 0; i < numToGen; i++)
    {
        Astronaut temp;
        bool valid = false;

        do
        {

            //get temporary position    
            temp.pos.x = (float)(rand() % (bnd * 2)) - bnd;
            temp.pos.y = (float)(rand() % (bnd * 2)) - bnd;
            if (!Collides(scrn.box, temp.pos))
            {
                valid = true;

                //check not too close to all other astronauts
                for (int j = 0; j < i; j++)
                {
                    //if closer than the min dist set invalid
                    if ((powf(temp.pos.x - astros[j].pos.x, 2) + powf(temp.pos.y - astros[j].pos.y, 2)) < AstronautConsts::minDistSq)
                    {
                        valid = false;
                    }
                }
            }
        } while (valid == false);


        temp.texNum = rand() % AstronautConsts::numTexs;
        temp.angle = (float)(rand() % 360);
        temp.turnSpeed = rand() % (2 * AstronautConsts::maxTurnSpeed) - AstronautConsts::maxTurnSpeed;

        temp.active = true;
        astros.push_back(temp);
    }

    numAstros = numToGen;
    numCollected = 0;

    compass.Initialise();
}
bool AstronautCollection::Update(float& elapsed, ForceObject& ship)
{
    Circle collider;
    collider.radius = AstronautConsts::collDist;

    for (int i = 0; i < numAstros; i++)
    {
        if (astros[i].active)
        {
            collider.centre = astros[i].pos;
            if (Collides(ship.hitbox, collider))
            {
                numCollected += 1;
                astros[i].active = false;
                
                Dim2D emitLoc = (astros[i].pos + ship.pos) / 2;
                CreateCollectionEmitter(emitLoc);
                
                if (numCollected == numAstros)
                {
                    return true;
                }
            }
            else
            {
                astros[i].Update(elapsed);
            }
        }
    }

    compass.Update(&astros, numAstros, ship.pos);

    return false;
}
void AstronautCollection::Draw(sf::RenderWindow& window, Screen& scrn)
{    
    Circle collider;
    collider.radius = AstronautConsts::collDist;

    for (int i = 0; i < numAstros; i++)
    {
        if (astros[i].active)
        {
            collider.centre = astros[i].pos;
            if (Collides(scrn.box, collider))
            {
                astroSpr.setTextureRect(ir[astros[i].texNum]);
                astroSpr.setRotation(astros[i].angle);
                scrn.DrawRelative(window, astroSpr, astros[i].pos);
            }
        }
    }

    compass.Draw(window);
}
void AstronautCollection::CreateCollectionEmitter(Dim2D pos)
{
    Emitter* em;
    em = pSys->CreateEmitter();

    if (em)
    {
        em->SetTexture(collPartTex, { 15,15 });
        em->SetColour((sf::Color)sf::Color::Yellow);
        em->initSpeed = { 360,361 };
        em->numAtOnce = 50;
        em->numToEmit = 100;
        em->pLifetime = 0.25;
        em->spawnRate = 0.04f;

        
        em->pos = pos;
    }
}