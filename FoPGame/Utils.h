#pragma once
#include <math.h>
#include <assert.h>
#define PI 3.14159265359

struct Dim2D
{
    float x, y;
    //function to normalise this as a vector, such that |Dim2D| = 1
    void normalise();
    //function to set this as a unit vector by means of specifying the angle between
    //this vector and the unit vector in the positive i direction
    void FromAngle(float& angle);
    //function to get the angle between this as a vector, and the unit vetor in the
    //positive i direction
    float GetAngle();

    inline Dim2D operator * (float v)
    {
        Dim2D temp;
        temp.x = x * v;
        temp.y = y * v;
        return temp;
    }

    inline Dim2D operator -()
    {
        Dim2D temp;
        temp.x = -x;
        temp.y = -y;
        return temp;
    }
    inline Dim2D operator -(Dim2D v)
    {
        Dim2D temp;
        temp.x = x - v.x;
        temp.y = y - v.y;
        return temp;
    }
    inline Dim2D operator +(Dim2D v)
    {
        Dim2D temp;
        temp.x = x + v.x;
        temp.y = y + v.y;
        return temp;
    }
    inline Dim2D operator /(float f)
    {
        Dim2D temp;
        temp.x = x / f;
        temp.y = y / f;
        return temp;
    }

};
