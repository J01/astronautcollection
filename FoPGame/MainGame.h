#pragma once
#include "SFML/Graphics.hpp"

#include "Asteroids.h"
#include "ForceObject.h"
#include "Utils.h"
#include "WorldZone.h"
#include "Screen.h"
#include "Astronauts.h"
#include "Particles.h"

//enumerates the different zones that the player can be in
enum Zone { PLAY, ASTEROID, DEAD, DEATH };
//return enum for the update function
enum ExitState { VICTORY, DEFEAT, NONE };

///////////////////////////////////////////////////////////////////////////////////////
//  Struct definition for the MainGame Struct                                        //
///////////////////////////////////////////////////////////////////////////////////////
struct MainGame
{
    //holds the asteroid field for the game
    AsteroidField astFld;
    //holds the ship for the game
    ForceObject ship;
    //holds the collection of astronauts
    AstronautCollection astronauts;
   
    Screen screen;

    //holds the zones that the ship is/was in
    Zone newMode = PLAY, lastMode = PLAY;
    
    ParticleSystem pSys;

    //holds whether the player has won (victory) / dead (dead)
    bool victory = false;
    bool dead = false;

    //holds the bounds for different zones
    World world;

    // function to complete initial setup of the struct
    void Initialise();
    // function to update the game
    ExitState Update(float&);
    // function to draw all components in the game
    void Draw(sf::RenderWindow&);
    // function to check if a dimension is in the bounds of a zone
    Zone GetZone(Dim2D&);

};

namespace GameConsts
{
    const Dim2D SCREEN_SIZE = { 800,800 };
}