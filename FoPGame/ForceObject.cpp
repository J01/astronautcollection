#include "ForceObject.h"

#include <math.h>
#include <assert.h>
#define PI 3.14159265359

void TeleportChargeBar::Initialise(Dim2D& size)
{
    //set textures
    if (!chargeBarTex.loadFromFile(path))
        assert(false);
    bottomBarSpr.setTexture(chargeBarTex);
    bottomBarSpr.setTextureRect(bottomBarTexRec);
    midBarSpr.setTexture(chargeBarTex);
    midBarSpr.setTextureRect(midBarTexRec);
    topBarSpr.setTexture(chargeBarTex);
    topBarSpr.setTextureRect(topBarTexRec);
    
    bottomIntSpr.setTexture(chargeBarTex);
    bottomIntSpr.setTextureRect(bottomIntTexRec);
    midIntSpr.setTexture(chargeBarTex);
    midIntSpr.setTextureRect(midIntTexRec);
    topIntSpr.setTexture(chargeBarTex);
    topIntSpr.setTextureRect(topIntTexRec);
    
    readySpr.setTexture(chargeBarTex);
    readySpr.setTextureRect(readyTexRec);

    //change origin of all items
    bottomBarSpr.setOrigin(bottomBarTexRec.width / 2, bottomBarTexRec.height / 2);
    midBarSpr.setOrigin(midBarTexRec.width / 2, midBarTexRec.height / 2);
    topBarSpr.setOrigin(topBarTexRec.width / 2, topBarTexRec.height / 2);
    bottomIntSpr.setOrigin(bottomIntTexRec.width / 2, bottomIntTexRec.height / 2);
    midIntSpr.setOrigin(midIntTexRec.width / 2, midIntTexRec.height / 2);
    topIntSpr.setOrigin(topIntTexRec.width / 2, topIntTexRec.height / 2);
    readySpr.setOrigin(readyTexRec.width / 2, readyTexRec.height / 2);

    Dim2D scaler;
    scaler = { bottomBarSize.x / bottomBarTexRec.width, bottomBarSize.y / bottomBarTexRec.height };
    bottomBarSpr.setScale(scaler.x, scaler.y);
    bottomIntSpr.setScale(scaler.x, scaler.y);
    scaler = { midBarSize.x / midBarTexRec.width, midBarSize.y / midBarTexRec.height };
    midBarSpr.setScale(scaler.x, scaler.y);
    midIntSpr.setScale(scaler.x, scaler.y);
    scaler = { topBarSize.x / topBarTexRec.width, topBarSize.y / topBarTexRec.height };
    topBarSpr.setScale(scaler.x, scaler.y);
    topIntSpr.setScale(scaler.x, scaler.y);
    scaler = { readySize.x / readyTexRec.width, readySize.y / readyTexRec.height };
    readySpr.setScale(scaler.x, scaler.y);


    Dim2D currPos;
    currPos = { offset.x, (size.y - offset.y) };
    currPos.y -= bottomBarSize.y;
    bottomBarSpr.setPosition(currPos.x + (bottomBarSize.x / 2), currPos.y + (bottomBarSize.y / 2));
    bottomIntSpr.setPosition(bottomBarSpr.getPosition().x, bottomBarSpr.getPosition().y);

    currPos.y -= (midBarSize.y + distBetBars);
    midBarSpr.setPosition(currPos.x + (midBarSize.x / 2), currPos.y + (midBarSize.y / 2));
    midIntSpr.setPosition(midBarSpr.getPosition().x, midBarSpr.getPosition().y);

    currPos.y -= (topBarSize.y + distBetBars);
    topBarSpr.setPosition(currPos.x + (topBarSize.x / 2) + 1, currPos.y + (topBarSize.y / 2));
    topIntSpr.setPosition(topBarSpr.getPosition().x, topBarSpr.getPosition().y);

    currPos.y -= (readySize.y + distBetBars);
    readySpr.setPosition(currPos.x + topBarSize.x / 2, currPos.y + (readySize.y) / 2);

}
void TeleportChargeBar::Update(float timeSince, const float timePerBar)
{
        if (timeSince >= 3 * timePerBar)
        {
            bottomIntSpr.setColor(bottomBarCol);
            midIntSpr.setColor(midBarCol);
            topIntSpr.setColor(topBarCol);
            readySpr.setColor(topBarCol);
        }
        else if (timeSince < timePerBar)
        {
            bottomIntSpr.setColor(emptyCol);
            midIntSpr.setColor(emptyCol);
            topIntSpr.setColor(emptyCol);
            readySpr.setColor(emptyCol);
        }
        else if (timeSince < 2 * timePerBar)
        {
            bottomIntSpr.setColor(bottomBarCol);
            midIntSpr.setColor(emptyCol);
            topIntSpr.setColor(emptyCol);
            readySpr.setColor(emptyCol);
        }
        else
        {
            bottomIntSpr.setColor(bottomBarCol);
            midIntSpr.setColor(midBarCol);
            topIntSpr.setColor(emptyCol);
            readySpr.setColor(emptyCol);
        }
}
void TeleportChargeBar::Draw(sf::RenderWindow& window)
{
    window.draw(topBarSpr);
    window.draw(bottomBarSpr);
    window.draw(midBarSpr);
    window.draw(topIntSpr);
    window.draw(midIntSpr);
    window.draw(bottomIntSpr);
    window.draw(readySpr);
}

void ForceObject::Initialise(Dim2D& scrn)
{
    speed = { 0,0 };
    pos = { 0, 0 };
    angle = 0;

    tex.loadFromFile("data/sprites/ship.png");
    spr.setTexture(tex);
    spr.setPosition(scrn.x / 2, scrn.y / 2);

    spr.setScale(ShipConsts::SHIP_SIZE.x / tex.getSize().x, ShipConsts::SHIP_SIZE.y / tex.getSize().y);
    spr.setOrigin(tex.getSize().x / 2.f, tex.getSize().y / 2.f);

    Dim2D temp = { pos.x - ShipConsts::SHIP_SIZE.x / 2, pos.y - ShipConsts::SHIP_SIZE.y / 2 };
    hitbox.Create(temp, (float&)(ShipConsts::SHIP_SIZE.x));

    tChargeBar.Initialise(scrn);
}
void ForceObject::Initialise(sf::Keyboard::Key fwd, sf::Keyboard::Key rght, sf::Keyboard::Key bck, sf::Keyboard::Key lft, sf::Keyboard::Key tel, Dim2D& scrn)
{
    Initialise(scrn);
    fwdKey = fwd;
    rghtKey = rght;
    bckKey = bck;
    lftKey = lft;
    telKey = tel;
}
void ForceObject::Update(float elapsed)
{
    assert(elapsed > 0);
    float moveElapsed = elapsed;

    //update the teleport timing
    timeSince += elapsed;
    tChargeBar.Update(timeSince, timePerBar);
    if (!tAvailable)
    {
        if (timeSince >= rechargeTime)
        {
            tAvailable = true;
        }
    
        //if in the teleport state
        if (inTeleport)
        {
            //if the time since activating has gone beyond animTime
            if (timeSince > animTime)
            {
                //then make the jump
                moveElapsed += jumpTime;
                inTeleport = false;
                emitterNeeded = true;
            }
        }
    }
  

    Dim2D fwdForce = { 0,0 };

    //Deal with turns
    if (sf::Keyboard::isKeyPressed(lftKey))
    {
        angle -= ShipConsts::TURN_SPEED * elapsed;
    }
    else if (sf::Keyboard::isKeyPressed(rghtKey))
    {
        angle += ShipConsts::TURN_SPEED * elapsed;
    }
    spr.setRotation(angle + 90);

    //Get the direction from the angle
    dir.x = (float)cos(angle * PI / 180);
    dir.y = (float)sin(angle * PI / 180);
    //dir.normalise();

    //Get the forces affecting the object
    if (sf::Keyboard::isKeyPressed(fwdKey))
    {
        fwdForce = dir * ShipConsts::FORWARD_FORCE;
    }
    else if (sf::Keyboard::isKeyPressed(bckKey))
    {
        fwdForce = -dir * ShipConsts::FORWARD_FORCE;
    }

    if (sf::Keyboard::isKeyPressed(telKey))
    {
        if (tAvailable)
        {
            tAvailable = false;
            inTeleport = true;

            timeSince = 0;
            CreateTeleportEmitters(pos);
        }
    }

    fwdForce = fwdForce - (speed / 2);

    //Update speed
    speed = speed + fwdForce * elapsed;
    Dim2D move = (speed * moveElapsed) - (fwdForce * (elapsed * elapsed) * 0.5f);
    pos = pos + move;
    hitbox.Translate(move);

    if (emitterNeeded)
    {
        CreateTeleportEmitters(pos);
        emitterNeeded = false;
    }
}
void ForceObject::Draw(sf::RenderWindow& window)
{
    window.draw(spr);
    tChargeBar.Draw(window);
}
void ForceObject::CreateTeleportEmitters(Dim2D& pos)
{
    Emitter* em;
    em = pSys->CreateEmitter();
    if (em)
    {
        em->pos = pos;
        em->initSpeed = { 40,41 };
        em->numToEmit = 600;
        em->numAtOnce = em->numToEmit / 3;
        em->pLifetime = 1.5f;
        em->spawnRate = 0.25f;
        em->SetColour(sf::Color(0, 127, 255, 200));

        em->scale = { 0.1f,0.1f };
    }
    em = pSys->CreateEmitter();
    if (em)
    {
        em->pos = pos;
        em->initSpeed = { 80,81 };
        em->numToEmit = 900;
        em->numAtOnce = em->numToEmit/3;
        em->pLifetime = 1.8f;
        em->spawnRate = 0.5f;
        em->SetColour(sf::Color(63, 224, 208, 150));

        em->scale = { 0.1f,0.1f };
    }
}