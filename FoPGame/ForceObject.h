#pragma once
#include "Utils.h"
#include "GameGeometry.h"
#include "Particles.h"
#include "SFML/Graphics.hpp"

struct TeleportChargeBar
{
    //much of this data could be loaded from file?
    // only needed during setup so would make sense to clear it
#pragma region ChargeBarSectionConsts
    //colors for the teleport recharge bar
    const sf::Color bottomBarCol = sf::Color::Yellow;
    const sf::Color midBarCol = sf::Color(255, 165, 0, 255);      //opaque orange
    const sf::Color topBarCol = sf::Color::Red;
    const sf::Color emptyCol = sf::Color(105,105,105,200);

    const std::string path = "data/sprites/teleportCharge.png";
    sf::Texture chargeBarTex;

    //tex rects and size of the charge bars
    const sf::IntRect bottomBarTexRec = sf::IntRect(0,104,44,60);
    const sf::IntRect midBarTexRec = sf::IntRect(0,52,84,56);
    const sf::IntRect topBarTexRec = sf::IntRect(0,0,120,56);

    const sf::IntRect topIntTexRec = sf::IntRect(120, 0, 120, 56);
    const sf::IntRect midIntTexRec = sf::IntRect(120, 52, 84, 56);
    const sf::IntRect bottomIntTexRec = sf::IntRect(120, 104, 44, 60);

    const sf::IntRect readyTexRec = sf::IntRect(240,0,220,88);

    const Dim2D bottomBarSize = { 65,80 };
    const Dim2D midBarSize = { 130,80 };
    const Dim2D topBarSize = { 195,80 };
    const Dim2D readySize = { 80,40};
#pragma endregion

    sf::Sprite bottomBarSpr;
    sf::Sprite midBarSpr;
    sf::Sprite topBarSpr;
    sf::Sprite bottomIntSpr;
    sf::Sprite midIntSpr;
    sf::Sprite topIntSpr;
    sf::Sprite readySpr;

    
    const float distBetBars = 1.5f;
    const Dim2D offset = { 20,20 }; //the distance of the bottom left corner of object 
                                    //from bottom left corner of screen

    //function to initialise the charge bar, taking the screen size as argument
    void Initialise(Dim2D& size);

    //function to update the sections of the bar based on the time since last teleport
    void Update(float timeSince, const float timePerBar);

    //function to draw the charge bar
    void Draw(sf::RenderWindow& window);
};

//Structure holding a basic object affected by forces
// it can be affected by keyboard input
struct ForceObject
{
    // Speed is a vector showing the change in position / second
    // pos is a vector showing the position of the object
    // dir is a vector pointing in the direction the object is facing
    Dim2D speed, pos, dir, drawPos;

    // the angle anti-clockwise from the vector in the positive i direction
    float angle;

    //Holds the key bindings for the movement of the object
    sf::Keyboard::Key fwdKey, lftKey, rghtKey, bckKey, telKey;

    // holds the sprite and texture of the ship
    sf::Sprite spr;
    sf::Texture tex;

    //particle system and hitbox
    Square hitbox;
    ParticleSystem* pSys;


    /*     Data for the teleport functionality     */        
    const float rechargeTime = 5.f;                 //minimum time between teleports
    const float timePerBar = rechargeTime / 3;
    float timeSince = 5.f;                          //time since last teleport (starts off able to teleport)
    TeleportChargeBar tChargeBar;

    const float animTime = 0.5f;                    //time between pressing the key and making the jump
    const float jumpTime = 3.5f;                    //the number of seconds worth of distance the ship travels
                                                    //dist teleported in facing direction = vel * jump time 
    bool inTeleport = false;
    bool tAvailable = true;
    bool emitterNeeded = false;


    // Function to initialise the object with no key bindings
    void Initialise(Dim2D&);
    // Function to initialise the object with key bindings
    // IN: Forward key, turn right key, back key, turn left key
    void Initialise(sf::Keyboard::Key fwd, sf::Keyboard::Key rght, sf::Keyboard::Key bck, sf::Keyboard::Key lft, sf::Keyboard::Key tel, Dim2D&);
    // Function to update the object
    // IN : elapsed
    // pre-condition : elapsed > 0
    void Update(float elapsed);
    // Function to draw the object
    // IN / OUT : RenderWindow
    void Draw(sf::RenderWindow&);
    //function to create the emitters made when beginning / ending a teleport
    void CreateTeleportEmitters(Dim2D& pos);
};


namespace ShipConsts
{
    // size of the ship
    const Dim2D SHIP_SIZE = { 100,100 };
    // number of degrees / second that will be turned
    //if a turn button is held
    const float TURN_SPEED = 60;
    // magnitude of the force added in the facing 
    //direction when the forward button is pressed
    const float FORWARD_FORCE = 100;
}