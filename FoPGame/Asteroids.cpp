#include "Asteroids.h"


bool Asteroid::Update(World* wld, float& elapsed)
{
    //move the asteroid
    Dim2D change;
    
    change.x = dir.x * v * elapsed;
    change.y = dir.y * v * elapsed;

    pos = pos + change;

    //rotate the asteroid
    angle = angle * vRot * elapsed;


    //check if the asteroid is in the dead zone
    if (wld->deadZone.isInZone(pos))
    {
        toBeRemoved = true;
    }

    return toBeRemoved;
}


void AsteroidField::Initialise(World& wld, Screen* screen, ForceObject* ship)
{
    srand(0);

    AsteroidZone = &wld.astZone;
    scrn = screen;
    this->ship = ship;

    asts.reserve(AsteroidConsts::numAsts);
    for (int i = 0; i < AsteroidConsts::numAsts; i++)
    {
        Asteroid newAst;
        asts.push_back(newAst);
        AstInitFirstTime(asts[i]);
    }
}
bool AsteroidField::Update(float& elapsed)
{
    bool remove = false, dead = false;
    Circle collider;
    
    for (int i = 0; i < AsteroidConsts::numAsts; i++)
    {
        remove = asts[i].Update(wld, elapsed);

        if (!dead)
        {
            collider.radius = AsteroidConsts::size.x;
            collider.centre = asts[i].pos;

            dead = Collides(ship->hitbox, collider);
        }

        if (remove && !dead)
        {
            if (!Collides(scrn->box, collider))
            {
                AstInit(asts[i]);
            }
        }
    }

    return dead;
}
void AsteroidField::Draw(sf::RenderWindow& window)
{
    for (int i = 0; i < AsteroidConsts::numAsts; i++)
    {

    }
}

void AsteroidField::AstInitFirstTime(Asteroid& ast)
{
    //can spend as long as we like here so brute force is okay
    Dim2D pos;

    do
    {
        pos.x = (rand() % (AsteroidZone->upperBound * 2)) - AsteroidZone->upperBound;
        pos.y = (rand() % (AsteroidZone->upperBound * 2)) - AsteroidZone->upperBound;
    } 
    while (Collides(scrn->box, pos));

    float angle = (rand() % 360) * (PI / 2);
    ast.pos = pos;
    ast.dir = { cosf(angle), sinf(angle) };

    angle = (rand() % 360);
    ast.angle = angle;
    float rotSpeed = GetRandomRotateSpeed();
    ast.vRot = rotSpeed;

    ast.v = GetRandomV();
}
void AsteroidField::AstInit(Asteroid& ast)
{

    ast.toBeRemoved = false;

    int big = (rand() % (AsteroidZone->upperBound - AsteroidZone->lowerBound)) + AsteroidZone->lowerBound;
    int small = (rand() % (AsteroidZone->upperBound * 2)) - AsteroidZone->upperBound;

    int area = rand() % 4;

    Dim2D pos;
    float angle = (rand() % 180) * (PI / 180);

    switch (area)
    {
    case 0:
        pos.x = (float)small;
        pos.y = -(float)big;
        break;
    case 1:
        pos.x = (float)big;
        pos.y = (float)small;

        angle -= (PI / 2);
        break;
    case 2:
        pos.x = (float)small;
        pos.y = (float)big;

        angle -= PI;
        break;
    case 3:
        pos.x = -(float)big;
        pos.y = (float)small;

        angle += PI / 2;
        break;
    }

    ast.pos = pos;
    ast.dir = { cosf(angle), sinf(angle) };

    angle = rand() % 360;
    ast.angle = angle;
    float rotSpeed = GetRandomRotateSpeed();
    ast.vRot = rotSpeed;

    ast.v = GetRandomV();
}

float AsteroidField::GetRandomV()
{
    float ran = (float)rand() / (float)RAND_MAX;
    float range = AsteroidConsts::maxSpeed - AsteroidConsts::minSpeed;
    
    ran = ran * range;
    ran = ran + AsteroidConsts::minSpeed;
    return ran;
}
float AsteroidField::GetRandomRadiansAngle()
{
    float ran = (float)rand() / (float)RAND_MAX;
    ran = ran * PI / 2;
    return ran;
}
float AsteroidField::GetRandomRotateSpeed()
{
    float ran = (float)rand() / (float)RAND_MAX;
    float range = AsteroidConsts::maxRotate - AsteroidConsts::minRotate;

    ran = ran * range;
    ran = ran + AsteroidConsts::minRotate;
    return ran;;
}