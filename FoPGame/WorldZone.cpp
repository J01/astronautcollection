#include "WorldZone.h";


bool WorldZone::isInZone(Dim2D& dim)
{
    //it is not in zone if it is outside the upper bounds or inside the lower bounds
    if ((fabs(dim.x) < lowerBound && fabs(dim.y) < lowerBound) || (fabs(dim.y) > upperBound && fabs(dim.x) > upperBound))
    {
        return false;
    }
    else
        return true;
}

void World::Init()
{
    //initialise all the world zones
    playZone.lowerBound = 0;
    playZone.upperBound = 1.5 * worldUnit;
    astZone.lowerBound = playZone.upperBound;
    astZone.upperBound = playZone.upperBound + 0.5 * worldUnit;
    deadZone.lowerBound = astZone.upperBound;
    deadZone.upperBound = astZone.upperBound + worldUnit;
    deathZone.lowerBound = deadZone.upperBound;
    deathZone.upperBound = 100000.f;
}