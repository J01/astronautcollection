#pragma once
#include "Utils.h"
#include "GameGeometry.h"

#include "SFML/Graphics.hpp"

#include <string>
#include <assert.h>

struct Screen
{
    Dim2D size, topLeft;
    sf::Sprite spr;
    sf::Texture tex;

    Square box;

    //function to initialise the screen
    //IN /OUT: size (Dim2D)
    // size defines the screen size, used to create the background
    void Initialise(Dim2D&);
    //function to update the screen
    //IN /OUT: ShipPos (Dim2D)
    // ShipPos defines the position of the ship
    void Update(Dim2D&);
    //function to drar the screen
    //IN / OUT: window (RenderWindow)
    void Draw(sf::RenderWindow&);
    //can be called by an object to be drawn on the screen relative to the world
    // IN/OUT: window to be drawn in, sprite of object to draw, and position of that object
    void DrawRelative(sf::RenderWindow&, sf::Sprite&, Dim2D&);

    //bool isOnScreen(Dim2D);
    //bool isOnScreen(Dim2D, Dim2D);
    //bool isOnScreen(Dim2D, float);
};

namespace ScreenConsts
{
    const std::string path = "data/background.png";
}