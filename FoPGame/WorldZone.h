#pragma once
#include "Utils.h"
#include "GameGeometry.h"

#define worldUnit 500

struct WorldZone
{
    int lowerBound;
    int upperBound;


    bool isInZone(Dim2D&);
};

struct World
{
    WorldZone playZone;
    WorldZone astZone;
    WorldZone deadZone;
    WorldZone deathZone;

    void Init();
};