#pragma once

///////////////////////////////////////////////////////////////////////////////////////
//  Header file containing structure definitions for asteroid and asteroid field     //
///////////////////////////////////////////////////////////////////////////////////////

#include <cstdlib>
#include <vector>

#include "SFML/Graphics.hpp"

#include "WorldZone.h"
#include "Utils.h"
#include "Screen.h"
#include "ForceObject.h"

#include "GameGeometry.h"

///////////////////////////////////////////////////////////////////////////////////////
//  Struct definition for the Asteroid Struct                                        //
///////////////////////////////////////////////////////////////////////////////////////
struct Asteroid
{
    // Dimensions holding the position of this asteroid and the direction in
    //which it is travelling
    Dim2D pos;
    Dim2D dir;
    float angle;

    // the velocity of this asteroid in the direction it is currently travelling
    float v;
    // the turn rate of the asteroid
    float vRot;

    // the time to take between sprite switches
    float timeBetChng;
    // the time since the last sprite change
    float timeSncChng;

    //the index of the current texture in the asteroidfield tex array
    int currTexNum;

    bool toBeRemoved = false;

    bool Update(World*, float&);
};

///////////////////////////////////////////////////////////////////////////////////////
//  Struct definition for the AsteroidField Struct                                   //
///////////////////////////////////////////////////////////////////////////////////////

//Asteroid field holds data about asteriods as a collective
struct AsteroidField
{
    // a vector holding all the asteroids that are part of this asteroid field
    std::vector<Asteroid> asts;
    sf::Texture texs;           //asteroid texture
    sf::Sprite spr;             //asteroid sprite

    WorldZone* AsteroidZone;    //Pointer to the zone in which asteroids can spawn
    World* wld;                 
    Screen* scrn;
    ForceObject* ship;

    void Initialise(World& wld, Screen* scrn, ForceObject* ship);
    bool Update(float&);
    void Draw(sf::RenderWindow&);

    void AstInitFirstTime(Asteroid& ast);
    void AstInit(Asteroid& ast);

    float GetRandomV();
    float GetRandomRadiansAngle();
    float GetRandomRotateSpeed();

};



///////////////////////////////////////////////////////////////////////////////////////
//  Namespace definition for asteroid related consts                                 //
///////////////////////////////////////////////////////////////////////////////////////
namespace AsteroidConsts
{
    // Size of asteroids
    const Dim2D size = { 10,10 };

    // Min and max speed of asteroids
    const float maxSpeed = 10.f;
    const float minSpeed = 5.f;
    
    // Min and max rotation rate of asteroids
    const float maxRotate = 0.f;
    const float minRotate = 10.f;

    // Number of asteroids to use in the game
    const int numAsts = 40;
}
