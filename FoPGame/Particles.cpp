#include "Particles.h"

//remember sprite just stores a reference to texture

////////////////////////////////////////////////////////////////////////
//Particle bank functions
void ParticleBank::Initialise()
{
    if (!defaultTexture.loadFromFile("data/particles/circle.png"))
        assert(false);
    
    //create a single particle loaded with the texture 
    Particle p;
    p.spr.setTexture(defaultTexture);

    //clear and create the particles vector
    particles.clear();
    particles.insert(particles.begin(), 5000, p); //insert 5000 ps in particles

    for (unsigned int i = 0; i < particles.size() - 1; i++)
    {
        //point each particles to the next one in the list
        particles[i].next = &particles[i + 1];
    }

    //point the free list to the start of the vector
    freeList = &particles[0];
    busyList = nullptr; //none in busy list
}
Particle* ParticleBank::Despawn(Particle *p, Particle *pPrev)
{
    Particle* next;

    //if not previous then the busy list points to p's next particles
    if (pPrev == nullptr)
    {
        busyList = p->next;
        next = busyList;
    }
    //otherwise previous next is p next
    else
    {
        pPrev->next = p->next;
        next = p->next;
    }
;
    p->next = freeList;
    freeList = p;

    //now reset p to default
    p->spr.setTexture(defaultTexture);
    p->spr.setScale(1, 1);
    p->spr.setTextureRect(sf::IntRect(0, 0, 32, 32));

    return next;
}
void ParticleBank::Update(float elapsed)
{
    Particle *curr = busyList;
    Particle *last = nullptr;

    //while we still have something we are pointing to
    while (curr)
    {
        //update lifetime of particle
        curr->life -= elapsed;

        if (curr->life <= 0)
        {
            //if it is dead then despawn and repoint current
            curr = Despawn(curr, last);
        }
        else
        {
            Dim2D* pos = &(curr->pos);
            (*pos) = (*pos) + curr->vel * elapsed;
            last = curr;
            curr = curr->next;
        }
    }
}
void ParticleBank::Draw(sf::RenderWindow& window, Screen& scrn)
{
    Particle *curr = busyList;
    while (curr)
    {
        scrn.DrawRelative(window, curr->spr, curr->pos);
        curr = curr->next;
    }
}

////////////////////////////////////////////////////////////////////////
//Emitter functions
Particle* Emitter::GetNewParticle(ParticleBank& bank)
{
    Particle *p = nullptr;

    //if any elements free in the list 
    if (bank.freeList)
    {
        p = bank.freeList;

        //move free pointer to the next particle in the list
        //then this particles next points to busy
        //we then point busy to this
        bank.freeList = p->next;
        p->next = bank.busyList;
        bank.busyList = p;

        //i.e. move from start of free to start of busy
    }
    return p;
}
void Emitter::Update(float elapsed, ParticleBank& bank)
{
    //update position of the emitter
    //increase the time between spawns
    //if the time has been enough create a new particles
    if (active)
    {
        pos.x += emitVel.x * elapsed;
        pos.y += emitVel.y * elapsed;

        lastEmit += elapsed;

        if (lastEmit >= spawnRate)
        {
            //spawn either num to spawn at once, or enough to make maximum
            int n;
            n = fmin(numAtOnce, numToEmit);

            //create n particles
            while (n)
            {
                //get a pointer to a free particle
                Particle *p = GetNewParticle(bank);
                if (p)
                {
                    p->life = pLifetime;
                    p->pos = pos;

                    float angle = (float)(rand() % 360);
                    //get speed with min + range(max - min)
                    float v = (float)(initSpeed.x + rand() % (int)(initSpeed.y - initSpeed.x));
                    p->vel = { cosf(angle) * v, sinf(angle) * v };

                    //should move with emitter
                    p->vel = p->vel + emitVel;
                    
                    //create the particle sprite
                    p->spr.setPosition(pos.x, pos.y);

                    if (pTex)
                    {
                        p->spr.setTexture(*pTex);
                        sf::IntRect fullSize;
                        fullSize.top = 0;
                        fullSize.left = 0;
                        fullSize.width = pTex->getSize().x;
                        fullSize.height = pTex->getSize().y;
                        p->spr.setTextureRect(fullSize);
                    }
                    if (colSet)
                    {
                        p->spr.setColor(colour);
                    }

                  

                    p->spr.setScale(scale.x, scale.y);

                    numToEmit--;                
                }
                n--;
            }
            lastEmit -= spawnRate;
        }
        //die if none left to produce
        if (numToEmit == 0)
            active = false;
    }
}
void Emitter::Reset()
{
    pos = { 0,0 };
    scale = { 1,1 };
    emitVel = { 0,0 };
    spawnRate = 0.01f;

    colSet = false;
    colour = sf::Color::White;

    pTex = nullptr;

    pLifetime = 1.f;
    numToEmit = 0;
    numAtOnce = 1;
    active = false;
}
void Emitter::SetTexture(sf::Texture& tex, Dim2D size)
{
    //point to the address of the texture we are passed
    pTex = &tex;
    
    //set the size
    scale.x = size.x / tex.getSize().x;
    scale.y = size.y / tex.getSize().y;
}
void Emitter::SetTexture(sf::Texture& tex)
{
    pTex = &tex;
}
void Emitter::SetColour(sf::Color& col)
{
    colour = col;
    colSet = true;
}

////////////////////////////////////////////////////////////////////////
//Emitter Bank functions
void EmitterBank::Initialise()
{
    Emitter em;

    emitters.clear();
    emitters.insert(emitters.begin(), 50, em);
    for (unsigned int i = 0; i < emitters.size() - 1; i++)
    {
        emitters[i].next = &emitters[i + 1];
    }
    freeList = &emitters[0];
    busyList = nullptr;
}
Emitter* EmitterBank::Despawn(Emitter* curr, Emitter* last)
{
    Emitter* next;

    if (last == nullptr)
    {
        busyList = curr->next;
        next = busyList;
    }
    else
    {
        last->next = curr->next;
        next = curr->next;
    }
    
    curr->next = freeList;
    freeList = curr;
    curr->Reset();

    return next;
}
Emitter* EmitterBank::Create()
{
    Emitter* em = freeList;

    if (em)
    {
        freeList = em->next;
        em->next = busyList;
        busyList = em;
        em->active = true;
    }

    return em;
}
void EmitterBank::Update(float elapsed, ParticleBank& bank)
{
    Emitter* last = nullptr;
    Emitter* curr = busyList;

    while (curr)
    {
        assert(curr != last);
        curr->Update(elapsed, bank);
        if (!curr->active)
        {
            curr = Despawn(curr, last);
        }
        else
        {
            last = curr;
            curr = curr->next;
        }
    }
}

////////////////////////////////////////////////////////////////////////
//Particle system functions
void ParticleSystem::Initialise()
{
    pBank.Initialise();
    eBank.Initialise();
}
void ParticleSystem::Update(float elapsed)
{
    pBank.Update(elapsed);
    eBank.Update(elapsed, pBank);
}
void ParticleSystem::Draw(sf::RenderWindow& window, Screen& scrn)
{
    pBank.Draw(window, scrn);
}
Emitter* ParticleSystem::CreateEmitter()
{
    return eBank.Create();
}


////////////////////////////////////////////////////////////////////////


//System explained
/*
 *   ParticleSystem manages the whole beast. There is only one particle system
 *   Emitter is an object that produces particles of a specified tex, size, vel etc.
 *   ParticleBank contains all of the particles both in use and not
 *   Particle is one particle in the system
 *
 *   There is a default texture for emitter particles, with this texture stored in ParticleSystem
 *   Free particles should always have this texture set
 *   When a particle is removed, the texture is reset
 */