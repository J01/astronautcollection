#pragma once
///////////////////////////////////////////////////////////////////////////////////////
//  System to handle particles                                                       //
///////////////////////////////////////////////////////////////////////////////////////
#include "SFML/Graphics.hpp"
#include "Utils.h"
#include "Screen.h"

#include <vector>
#include <math.h>

///////////////////////////////////////////////////////////////////////////////////////
//  Structure holding one particle                                                   //
///////////////////////////////////////////////////////////////////////////////////////
struct Particle
{
    //pointer to the the next particle in the particles list
    Particle *next = nullptr;
    //sprite for the particle
    sf::Sprite spr;
    //the time remaining before this particle should despawn
    float life = 0;
    //velocity of the particle
    Dim2D vel;
    //position of the particle in the game world
    Dim2D pos;
};


///////////////////////////////////////////////////////////////////////////////////////
//  Structure holding a collection of all the particles                              //
//  Manages two lists, one of free particles that can be claimed by various emitters //
//  and one of all the particles that are in use                                     //
///////////////////////////////////////////////////////////////////////////////////////
struct ParticleBank
{
    //a vector of all the particles included in the system
    std::vector<Particle> particles;
    
    // pointers to the beginning of each particle list
    Particle *busyList = nullptr;
    Particle *freeList = nullptr;

    sf::Texture defaultTexture;

    //function to initialise the particle bank
    void Initialise();
    //move a particle from busy to free
    Particle* Despawn(Particle *p, Particle *prev);
    //update all particles that are currently in use
    void Update(float elapsed);
    //draw all particles that are still alive relative to the screen
    void Draw(sf::RenderWindow& window, Screen& scrn);

};


///////////////////////////////////////////////////////////////////////////////////////
//  Structure for a particle emitter                                                 //
///////////////////////////////////////////////////////////////////////////////////////
struct Emitter
{
    Emitter* next;

    //the position of the emitter in the game world
    Dim2D pos;
    //the minimum and maximum size of the particle
    Dim2D scale = { 1,1 };
    //the velocity of the emitter
    Dim2D emitVel = { 0,0 };

    //the minimum and maximum speed of each particle
    Dim2D initSpeed;

    //the desired time between particle spawns (in seconds)
    float spawnRate = 0.01f;

    bool colSet = false;
    sf::Color colour = sf::Color::White;

    sf::Texture *pTex = nullptr;
    
    //life time of each particle
    float pLifetime = 1.f;
    //the number of particles this emitter is supposed to create
    int numToEmit = 0;
    //the number of particles to fire off with each spawn
    int numAtOnce = 1;
    //whether or not the emitter is alive
    bool active = false;

    //time since last particle emission
    float lastEmit = 0.f;

    //Check the bank for free particles; if there is one, claim it for this emitter
    //returns a pointer to the particle to be claimed
    Particle* GetNewParticle(ParticleBank& bank);
    //Update the emitter, handling spawning of new particles and despawning of emitter
    void Update(float elapsed, ParticleBank& bank);
    //reset the Emitter
    void Reset();

    //function to set the texture and size of the particles emitted from this
    void SetTexture(sf::Texture& tex, Dim2D size);
    //function to set the texture of particles emitted from this emitter
    void SetTexture(sf::Texture& tex);
    //function to set the colour of these particles
    void SetColour(sf::Color& col);
};

///////////////////////////////////////////////////////////////////////////////////////
//  Structure for an emitter bank                                                    //
///////////////////////////////////////////////////////////////////////////////////////
struct EmitterBank
{
    std::vector<Emitter> emitters;
    Emitter* freeList = nullptr;
    Emitter* busyList = nullptr;

    //function to create the cache of emitters
    void Initialise();
    //function to despawn an emitter
    Emitter* Despawn(Emitter *curr, Emitter *prev);
    //function to create a new emitter
    Emitter* Create();
    //run updates on all emitters that are active
    void Update(float elapsed, ParticleBank& bank);
};



///////////////////////////////////////////////////////////////////////////////////////
//  Structure for a particle emitter                                                 //
//  Contains a cache of particles and a cache of emitters
///////////////////////////////////////////////////////////////////////////////////////
struct ParticleSystem
{
    //bank for emitters and particles
    ParticleBank pBank;
    EmitterBank eBank;

    //setup the system
    void Initialise();
    //update whole system
    void Update(float elapsed);
    //draw the whole particle system
    void Draw(sf::RenderWindow& window, Screen& scrn);
    //create a new Emitter
    Emitter* CreateEmitter();
};
