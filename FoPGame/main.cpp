 #include <assert.h>
#include <string>

#include "SFML/Graphics.hpp"
#include "MainGame.h"



using namespace std;
using namespace sf;



int main()
{
	sf::RenderWindow window(sf::VideoMode((unsigned int)GameConsts::SCREEN_SIZE.x, (unsigned int)GameConsts::SCREEN_SIZE.y), "Hangman");


    MainGame game;
    game.Initialise();

    Clock clk;
    float elapsed;


	// Start the game loop 
	while (window.isOpen())
	{
		// Process events
		sf::Event event;
		while (window.pollEvent(event))
		{
			// Close window: exit
			if (event.type == sf::Event::Closed) 
				window.close();

		} 

		// Clear screen
		window.clear();

        elapsed = clk.getElapsedTime().asSeconds();
        clk.restart();

        game.Update(elapsed);
        game.Draw(window);

		// Update the window
		window.display();
	}

	return EXIT_SUCCESS;
}
