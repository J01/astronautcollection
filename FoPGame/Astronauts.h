#pragma once
#include <vector>
#include <algorithm>
#include <string>

#include "Utils.h"
#include "Screen.h"
#include "WorldZone.h"
#include "ForceObject.h"
#include "Particles.h"

#include "SFML/Graphics.hpp"

namespace AstronautConsts
{
    const int numTexs = 4;
    const std::string basePath = "data/sprites/astronauts.png";
    
    //size of the astronaut in the world
    const Dim2D AstronautSize = { 40,50 };
    //size of the astronaut on the sprite sheet
    const Dim2D texSize = { 20, 30 };

    const int collDist = 30;
    const int maxTurnSpeed = 30;

    //the minimum distance between generated astronauts, squared
    const float minDistSq = 250000.f;
}


struct Astronaut
{
    float angle = 0;
    int turnSpeed = 0;

    int texNum = 0;
    Dim2D pos = { 0,0 };

    bool active = true;

    //function to update the astronaut, by rotating
    // IN / OUT : time in last frame
    // pre-condition : elapsed > 0
    // post-condition: 0 <= angle <= 2pi
    void Update(float& elapsed);
};

struct AstroCompass
{
    sf::Texture tex;
    sf::Sprite spr;

    const Dim2D size = { 40,60 };
    const Dim2D centrePos = { 80,80 };

    void Initialise();
    void Update(std::vector<Astronaut>* astros, int num, Dim2D& ship);
    void Draw(sf::RenderWindow& window);

};


struct AstronautCollection
{
    int numAstros = 0;
    int numCollected = 0;

    const float minDist = 30.f;

    sf::Sprite astroSpr;
   
    //array of textures, of length defined in namespace
    sf::Texture sprSheet;
    //array of IntRects
    sf::IntRect ir[AstronautConsts::numTexs];

    //vector of astronauts
    std::vector<Astronaut> astros;


    ParticleSystem* pSys;
    sf::Texture collPartTex;

    AstroCompass compass;


    //function to initialise the astronautcollection
    //IN/OUT : Screen, Number of astronauts to generate, World
    //pre-condition : numToGen > 0, Screen and World != null
    //post-condition: numToGen astronauts have been created
    void Initialise(Screen&, int&, World&);
    
    //function to Update the astronautcollection
    //IN/OUT : time in last frame, Ship object
    //pre-condition : elapsed > 0
    //post-condition: true returned if all astronauts have been collected
    bool Update(float&, ForceObject&);

    //function to Draw the astronautCollection
    //IN/OUT : RenderWindow, Screen object
    void Draw(sf::RenderWindow&, Screen&);

    //function to setup a particle emitter created when an astronaut is collected
    //IN/OUT : the astronaut that has been collected
    void CreateCollectionEmitter(Dim2D pos);
   
};

