#include "Utils.h";

void Dim2D::normalise()
{
    float magnitude = (x * x) + (y * y);
    magnitude = sqrt(magnitude);

    x = x / magnitude;
    y = y / magnitude;
}

void Dim2D::FromAngle(float& angle)
{
    Dim2D temp;
    assert(angle <= 2 * PI);

    if (angle < PI / 2)
    {
        temp.x = cos(angle);
        temp.y = sin(angle);
    }
    else if (angle <= PI)
    {
        temp.y = cos(angle - (PI / 2));
        temp.x = -sin(angle - (PI / 2));
    }
    else if (angle <= 3 * PI / 2)
    {
        angle = angle - (2 * PI);
        temp.y = cos(angle + (PI / 2));
        temp.x = -sin(angle - (PI / 2));
    }
    else
    {
        angle = angle - (2 * PI);
        temp.x = cos(angle);
        temp.y = sin(angle);
    }

    x = temp.x;
    y = temp.y;
}

float Dim2D::GetAngle()
{
    float tempAng = atan(y / x);

    if (x < 0)
    {
        if (y > 0)
        {
            tempAng += (PI);
        }
        else if (y < 0)
        {
            tempAng -= (PI);
        }
    }

    return tempAng;
}
