#include "MainGame.h"

void MainGame::Initialise()
{
    world.Init();

    Dim2D temp;
    temp.x = GameConsts::SCREEN_SIZE.x;
    temp.y = GameConsts::SCREEN_SIZE.y;

    ship.Initialise(sf::Keyboard::Key::W, sf::Keyboard::Key::D, sf::Keyboard::Key::S, sf::Keyboard::Key::A, sf::Keyboard::Key::Space, temp);
    screen.Initialise(temp);
    
    //asteroidField.Initialise(World)
    int param = 8;
    astronauts.Initialise(screen, param, world);

    pSys.Initialise();
    ship.pSys = &pSys;
    astronauts.pSys = &pSys;
}
ExitState MainGame::Update(float& elapsed)
{
    ship.Update(elapsed);
    // dead = asteroidField.Update(elapsed, World)
    screen.Update(ship.pos);
    victory = astronauts.Update(elapsed, ship);
    pSys.Update(elapsed);
    return NONE;
}
void MainGame::Draw(sf::RenderWindow& window)
{
    screen.Draw(window);
    astronauts.Draw(window, screen);
    pSys.Draw(window, screen);
    ship.Draw(window);
}
Zone MainGame::GetZone(Dim2D& dim)
{
    return PLAY;
}


